/********************************************************************************
Copyright (C) 2014 Shenzhen Exsaf Electronics Co., Ltd.
*
* File name: display_driver.c
* Summary: nxp pcf85133 LCD controller driver
* other instructions:
* Author: Gokhan KOC
* Completion Date:
*
* Modify record 1:
* Modified Date:
* version number :
* Modifier:
* Modification:
***********************************************************************************/

#include "stm32f1xx_hal.h"

#define    a    0x01
#define    b    0x02
#define    c    0x04
#define    d    0x08
#define    e    0x10
#define    f    0x20
#define    g    0x40

#define    Poit 0x80

const unsigned char segment7_decoded_table[40] =
{
    a+b+c+d+e+f,    //"0"
    b+c,        //"1"
    a+b+d+e+g,      //"2"
    a+b+c+d+g,      //"3"
    b+c+f+g,            //"4"
    a+c+d+f+g,            //"5"
    a+c+d+e+f+g,    //"6"
    a+b+c,            //"7"
    a+b+c+d+e+f+g,      //"8"
    a+b+c+d+f+g,    //"9"  --9
    
    a+b+c+e+f+g,        //"A"   10
    c+d+e+f+g,        //"b"
    a+d+e+f,              //"C"
    b+c+d+e+g,       //"d"
    a+b+d+e+f+g,        //"e"
    a+e+f+g,            //"F"  15
    a+d+e+f+g,          //"E"  16
    b+c+e+f+g,          //"H"
    b+c,                  //"i"
    b+c+d,              //"J"  19

    0x00,                //"k" 
    d+e+f,              //"L"  21
    a+b+c+e+f,           //"N"
    c+e+g,               //"n"
    c+d+e+g,            //"o"
    a+b+e+f+g,          //"P"
    a+b+c+d+e+f,         //"O"
    e+g,                //"r"  27
    a+c+d+f+g,            //"S"
    d+e+f+g,            //"t"  29
    
    b+c+d+e+f,          //"U"
    b+c+d+e+f,                //"V" 
    b+c+d+e+f,               //"W" 32
    0x00,               //"X"
    b+c+d+f+g,          //"y"
    0x00,               //"Z" 
    0x00,               //"NULL" 36
    0xff,               //"FULL" 37
    g,                   //"minus_sign" 38           
    a+c+d+e+f         // g 39
};

unsigned char gauc_display_table[40];
unsigned char CODE_2841[11] = {0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f,0x00}; //0~9Display the code

#define short_delay()  {asm("nop");}    //nop()


/*******************************************
* Function name: iic_init
* Function: IIc initialization
* Entry parameters:
* Export parameters:
* return value:
********************************************/
void iic_init(void)
{
    LCD_IIC_SDA_OUT;   
    LCD_IIC_SCL_OUT;
    LCD_IIC_SCL_HIGH();
    LCD_IIC_SDA_HIGH();
    
}


/***************************************
* Function name: iic_start
* Function function: IIc communication start bit output
* Entry parameters:
* Export parameters:
* return value:
****************************************/
void iic_start(void)                 
{
     LCD_IIC_SDA_HIGH();    //IIcSda=1;
    short_delay();
    LCD_IIC_SCL_HIGH();    //IIcScl=1;
    short_delay();
    LCD_IIC_SDA_LOW();     //IIcSda=0;
    short_delay();
    LCD_IIC_SCL_LOW();     //IIcScl=0;
    short_delay();
    short_delay();
}


/*****************************************
* Function name: iic_stop
* Function function: IIc communication stop bit output
* Entry parameters:
* Export parameters:
* return value:
*******************************************/
void iic_stop(void)                 
{
    LCD_IIC_SDA_LOW();     //IIcSda=0;
    short_delay();
    LCD_IIC_SCL_HIGH();    //IIcScl=1;
    short_delay();
    LCD_IIC_SDA_OUT;
    short_delay();
    LCD_IIC_SDA_HIGH();    //IIcSda=1;
    short_delay();
    LCD_IIC_SCL_LOW();     //IIcScl=0;
    short_delay();
    short_delay();    
}


/***********************************************************************************
* Function name: iic_send_byte
* Function: IIc communication sends a byte of data
* Input parameters: unsigned char dbyte The data is sent via IIc communication
* Output parameters:
* return value:
***********************************************************************************/
void iic_send_byte(unsigned char dbyte)               
{
    unsigned char i;
    
    LCD_IIC_SDA_OUT;
    for (i = 0; i < 8; i++)
    {
        if (dbyte & 0x80)
        {
            LCD_IIC_SDA_HIGH();   //IIcSda=1;
        }
        else
        {
            LCD_IIC_SDA_LOW();    //IIcSda=0;
        }
        dbyte <<= 1;
        short_delay();
        LCD_IIC_SCL_HIGH();       //IIcScl=1;
        short_delay();       
        LCD_IIC_SCL_LOW();        //IIcScl=0;        
         short_delay(); 
    }

    LCD_IIC_SDA_IN();   
    short_delay();
    LCD_IIC_SCL_HIGH();           //IIcScl=1;
    short_delay();
    LCD_IIC_SCL_LOW();            //IIcScl=0;   
    short_delay();
    short_delay();
    LCD_IIC_SDA_OUT;      
}


/************************************************************************
* Function name: lcd_init
* Functional Description: Initialization
* Input parameters:
* Output parameters:
* return value :
*************************************************************************/
void lcd_init(void)
{  
    iic_init();
    iic_start();
	
    if (gst_detector_config_info.uc_detector_type == ESD3000_LED)
    {
        //Digital Tube
        iic_send_byte(0x48);
    	iic_send_byte(0x51);     //Initialized to 5 grayscale, open display, 8-bit display
    }
    else
    {
        iic_send_byte(0x70);     // From device address and write operation
        iic_send_byte(0x80);     // Continuous write command
        iic_send_byte(0xC8);     // Enable display
        iic_send_byte(0x80);     // Continuous write command
        iic_send_byte(0xE0);     // Device address
        iic_send_byte(0x00);     // The last one to write the order
        iic_send_byte(0xF4);     // Do not blink
    }
	
    iic_stop();   
}


/************************************************************************
* Function name: lcd_refresh
* Function Description: Refresh the display RAM
* Input parameters:
* Output parameters:
* return value :
*************************************************************************/
void lcd_refresh(unsigned char *ptr)
{
    unsigned char i;
  
    iic_init();
    iic_start();
    iic_send_byte(0x70);     // From device address and write operation
    iic_send_byte(0x80);     // Continue to write commands
    iic_send_byte(0x0C);     // Start section S12
    iic_send_byte(0x40);     // Write data
    
    for (i = 0; i < 30; i++)
    {
        iic_send_byte(*(ptr+i));     // Write RAM in bytes
    }
    
    iic_stop();       
}

/************************************************************************
* Function name: led_refresh
* Function Description: Refresh the display RAM
* Input parameters:
* Output parameters:
* return value : 
*************************************************************************/
void led_refresh(unsigned char *ptr)
{
    iic_init();
    iic_start();
    iic_send_byte(0x68);     // 
    iic_send_byte(*(ptr));     //
    iic_stop();
	
	iic_init();
    iic_start();
    iic_send_byte(0x6A);     //
    iic_send_byte(*(ptr+1));     //
    iic_stop();
	
	iic_init();
    iic_start();
    iic_send_byte(0x6C);     // 
    iic_send_byte(*(ptr+2));     //;
    iic_stop();
	
	iic_init();
    iic_start();
    iic_send_byte(0x6E);     //
    iic_send_byte(*(ptr+3));     //    
    iic_stop();       
}

void lcd_refresh_update(void)
{
    if (gst_detector_config_info.uc_detector_type == ESD3000_LED)
    {
        led_refresh(gauc_display_table);
    }
    else
    {
        lcd_refresh(gauc_display_table);
    }
}

/************************************************************************
* Function name: display_all
Find composition views Rhithers.
* Input parameters: unsigned char onoff, 0 off, 1 significant
* Output parameters:
* return value :
*************************************************************************/
void display_all(unsigned char onoff)
{  
    unsigned char i;
  
    for (i = 0; i < 40; i++)
    {
        gauc_display_table[i] = 0xFF * onoff;
    }      
    //lcd_refresh(gauc_display_table);
}

/************************************************************************
* Function name: display_esd3000_all
* Function description: lit esd3000 all used areas
* Input parameters:
* Output parameters:
* return value :
*************************************************************************/
void display_esd3000_all(void)
{
    unsigned char i;
    
    gauc_display_table[0] |= ICON_H;
    gauc_display_table[0] |= ICON_L;
    gauc_display_table[0] |= ICON_F;

    gauc_display_table[17] |= ICON_COMM;
    gauc_display_table[17] |= ICON_SCALE;

    for (i = 1; i < 6; i++)
    {
        gauc_display_table[i] = 0xFF;
    } 

    for (i = 20; i < 26; i++)
    {
        gauc_display_table[i] = 0xFF;
    } 

    gauc_display_table[17] |= ICON_PERCENT;
    gauc_display_table[6] |= ICON_MILLESIMAL;  // Lit one thousandth mark
    gauc_display_table[6] |= ICON_PPM;
}
/************************************************************************
* Function name: clear_display_buf
* Function Description: Clear the display array
* Input parameters:
* Output parameters:
* return value :
*************************************************************************/
/*void clear_display_buf(void)
{
    memset(gauc_display_table, 0, sizeof(gauc_display_table));
}*/


/************************************************************************
* Function name: display_seg7
* Functional description: 7-segment display
* Input parameters: unsigned char position: display position, according to the LCD label
* unsigned char num: the character to be displayed
* Output parameters:
* return value :  
*************************************************************************/
void display_seg7_num(unsigned char position, unsigned char num)
{
    //if(num<10)
    {
        gauc_display_table[position] = seg7_decode_table[num][1];
    }    
}

/************************************************************************
* Function name: display_seg16
* Functional description: 16-zone display
* Input parameters: unsigned char position: display position, according to the LCD label
* unsigned char ascii_code: character ASCII
* Output parameters:
* return value :  
*************************************************************************/
/*void display_seg16(unsigned char position, unsigned char ascii_code)
{
    unsigned char i,j;
    
    if (ascii_code <= 9)        
    {
        ascii_code += 0x30;     // Converts the number to ASCII
    }
    
    j = sizeof(seg16_decode_table) / sizeof(seg16_decode_table[0]);
    for (i = 0; i < j; i++)
    {
        if (seg16_decode_table[i][0] == ascii_code)    
        {
            break;
        }
    }
    
    if (i == j)       // Not the character code in the list
    {
        return;
    }
    
    if (position < 11)
    {
        j = position * 2 - 5;
    }
    else
    {
        j = position * 2 - 2;
    }
    gauc_display_table[j] = seg16_decode_table[i][1] >> 8;   
    gauc_display_table[j + 1] = seg16_decode_table[i][1];
}*/


/************************************************************************
* Function name: display_icon_h
* Function Description: Displays the H icon
* Input parameters:
* Output parameters:
* return value :
*************************************************************************/
void display_icon_h(void)
{
    gauc_display_table[0] |= ICON_H;
}


/************************************************************************
* Function name: display_icon_l
* Function Description: Displays the L icon
* Input parameters:
* Output parameters:
* return value :
*************************************************************************/
void display_icon_l(void)    
{
    gauc_display_table[0] |= ICON_L;
}

/************************************************************************
* Function name: display_icon_f
* Function Description: Displays the F icon
* Input parameters:
* Output parameters:
* return value :
*************************************************************************/
void display_icon_f(void)
{
    gauc_display_table[0] |= ICON_F;
}


/*********************************************************
* Function name: display_icon_comm
* Function Description: Displays the communication flag
* Input parameters:
* Output parameters:
* return value :
*********************************************************/
void display_icon_comm(void)     
{
    gauc_display_table[17] |= ICON_COMM;
}


/*********************************************************
* Function name: display_icon_message
* Function Description: Displays the message flag
* Input parameters:
* Output parameters:
* return value :
*********************************************************/
/*void display_icon_message(void)     
{
    gauc_display_table[17] |= ICON_MESSAGE;
}*/


/*********************************************************
* Function name: display_icon_scale
* Function Description: Displays the calibration mark
* Input parameters:
* Output parameters:
* return value :
*********************************************************/
void display_icon_scale(void)     
{
    gauc_display_table[17] |= ICON_SCALE;
}


/*********************************************************
* Function name: display_icon_clock
* Function Description: Displays the clock mark
* Input parameters:
* Output parameters:
* return value :
*********************************************************/
/*void display_icon_clock(void)     
{
    gauc_display_table[17] |= ICON_CLOCK;
}*/


/*********************************************************
* Function name: display_icon_fan
* Function Description: Displays the fan flag
* Input parameters:
* Output parameters:
* return value :
*********************************************************/
void display_icon_fan(void)     
{
    gauc_display_table[17] |= ICON_FAN;
}


/************************************************************************
* Function name: display_point
* Function Description: Seven-segment display decimal point
* Input parameters: unsigned char point: 1-4 decimal places, 0 does not show the decimal point
* Output parameters:
* return value : 
*************************************************************************/
void display_point(unsigned char point)
{
    if ((point > 0) && (point < 5))
    {
        gauc_display_table[6 - point] |= 0x10; 
    }
}

/************************************************************************
* Function name: display_minus
* Functional Description: Seven section shows a negative sign
* Input parameters:
* Output parameters:
* return value : 
*************************************************************************/
void display_minus(void)
{
    if (gst_detector_config_info.uc_detector_type == ESD3000_LED)
    {
        gauc_display_table[0] = segment7_decoded_table[minus_sign];
    }
    else
    {
        gauc_display_table[1] |= 0x10;
    }
}
/************************************************************************
* Function name: display_str7
* Function Description: Seven-segment display string
* Input parameters: char * pstr: string, 5 characters, do not show the use of spaces
* Output parameters:
* return value :
*************************************************************************/
/*void display_str7(char *pstr)
{
    unsigned char i;
    
    for (i = 0; i < 5; i++)
    {
        display_seg7((i + 1), *(pstr + i));
    } 
}*/


/************************************************************************
* Function name: display_str7_err
* Function Description: Segment display Err01 or Err02
* Input parameters: unsigned char num: 1 or 2
* Output parameters:
* return value :
*************************************************************************/
void display_str7_err(unsigned char num)
{
	display_seg7_num(1,char7_E);
	display_seg7_num(2,char7_r);
	display_seg7_num(3,char7_r);
	display_seg7_num(4,0);
	display_seg7_num(5,num);
}

/************************************************************************
* Function name: display_version
* Functional Description: Seventh area display version v1.7
* Input parameters:
* Output parameters:
* return value :
*************************************************************************/
void display_version(void)
{
    if (gst_detector_config_info.uc_detector_type == ESD3000_LED)
    {
        Display2_2841(100, 2);
        gauc_display_table[0] = segment7_decoded_table[char_u];
    }
    else
    {
        display_seg7_num(1,char7_no_seg);
        display_seg7_num(2,char7_U);
        display_seg7_num(3,0);
        display_seg7_num(4,3);
        display_seg7_num(5,1);
        display_point(2);
    }
}


/************************************************************************
* Function name: display_version_real
* Functional Description: 16 paragraphs left show the actual version, 20161010 b5
* Input parameters:
* Output parameters:
* return value :
*************************************************************************/
void display_version_real(void)
{

    if (gst_detector_config_info.uc_detector_type == ESD3000_LED)
    {
        gauc_display_table[0] = segment7_decoded_table[no_seg];
        gauc_display_table[1] = segment7_decoded_table[no_seg];
        gauc_display_table[2] = segment7_decoded_table[char_b];
        gauc_display_table[3] = segment7_decoded_table[6];
    }
    else
    {
        display_all(0);	
        display_seg16_num(6, char16_b);
        display_seg16_num(7, 6);           
    }
	
	lcd_refresh_update();
	occupy_cpu_one_second();
	display_all(0);		
}

/************************************************************************
* Function name: display_num7
* Function Description: Seven-segment display value
* Input parameters: unsigned int num: The displayed value
* Output parameters:
* return value :
*************************************************************************/
void display_num7(unsigned int num)
{
    unsigned char i;
    unsigned int temp;
    
    for (i = 0; i < 4; i++)
    {
        
        if(num>0)
        {
            temp=num%10;
            display_seg7_num((4-i),temp);
            num/=10;
        }
        else if(i<gst_detector_config_info.uc_decimals+1)
        {
            display_seg7_num((4-i),0);
        }
        else
        {
            break;
        }
    } 
}

/************************************************************************
* Function name: display_seg16_num
* Functional description: 16 area a display value
* Input parameter: unsigned char position: position unsigned char num: the displayed value
* Output parameters:
* return value :
*************************************************************************/
void display_seg16_num(unsigned char position, unsigned char num)
{
    unsigned char j=0;
    
    //if(num<10)
    {
        if (position < 11)
        {
            j = position * 2 - 5;
        }
        else
        {
            j = position * 2 - 2;
        }
        gauc_display_table[j] = seg16_decode_table[num][1] >> 8;   
        gauc_display_table[j + 1] = seg16_decode_table[num][1];
        //gauc_display_table[position] = seg16_decode_table[num][1];
    }    
}

/************************************************************************
* Function name: display_num16
* Functional Description: 16 area left area shows the value
* Input parameters: unsigned int num: The displayed value
* Output parameters:
* return value :
*************************************************************************/
void display_num16(unsigned int num)
{
    unsigned char i;
    unsigned int temp;
    
    for (i = 0; i < 5; i++)
    {
        
        if(num>0)
        {
            temp=num%10;
            display_seg16_num((5-i+5),temp);
            num/=10;
        }
        /*else if(i<gst_detector_config_info.uc_decimals+1)
        {
            display_seg7_num((4-i),0);
        }*/
        else
        {
            display_seg16_num((4-i+6),0);
            break;
        }
    } 
}

/************************************************************************
* Function name: display_eights
* Functional description: seven sections show n 8
* Input parameters: unsigned int num: Displays the number of 8
* Output parameters:
* return value :
*************************************************************************/
void display_eights(unsigned char num)
{
	unsigned char i;

    for(i=1;i<=num;i++)
    {
		display_seg7_num(i,8);
    }
}

/************************************************************************
* Function name: display_unit
* Function description: display unit
* Input parameters: unsigned char unit: unit code, follow the HART protocol specification
* Output parameters:
* return value :
*************************************************************************/
void display_unit(unsigned char unit)     
{
    unsigned char i,j;
       
    for (i = 0; i < 5; i++)
    {
        display_seg16_num(i+11,UNIT_DATA[unit][i]);
    }
  
    /*************** Lit%, cube, square, PPM, ohm and other special signs ******************/
    switch(unit)
    {   
	    case UNIT_VV:
	        gauc_display_table[17] |= ICON_PERCENT;    // Lit %
	        break; 
		  
	    case UNIT_LEL:
	        gauc_display_table[17] |= ICON_PERCENT;    // Lit %
	        break;
		  
	    case UNIT_KPPM:
	        gauc_display_table[6] |= ICON_MILLESIMAL;  // Lit one thousandth mark
	        break;   
		  
	    case UNIT_PPM:
	        gauc_display_table[6] |= ICON_PPM;         // Lit one million mark
	        break; 
		  
	    default:
	        break;
	    }     
}

/************************************************************************
* Function name: Display_2841
* Function description: digital display without decimal digits
* Input parameters:
* Output parameters:
* return value :
*************************************************************************/
void Display_2841(unsigned int dat) //Digital display
{
    unsigned char temp[4];

	temp[0] = dat/1000%10;
    temp[1] = dat/100%10;
    temp[2] = dat/10%10;
    temp[3] = dat%10;
        
    if(temp[0] == 0)
    {        
        gauc_display_table[0] = segment7_decoded_table[no_seg];    //This bit is blank       
    }else
    {
        gauc_display_table[0] = segment7_decoded_table[temp[0]];
    }
	gauc_display_table[1] = segment7_decoded_table[temp[1]];
	gauc_display_table[2] = segment7_decoded_table[temp[2]];
	gauc_display_table[3] = segment7_decoded_table[temp[3]];
}
/************************************************************************
* Function name: Display_2841
* Function Description: digital display with a decimal point number
* Input parameters:
* Output parameters:
* return value :
*************************************************************************/
void Display2_2841(unsigned int dat, unsigned char add) //Digital display
{
    unsigned char temp[4];

	temp[0] = dat/1000%10;
    temp[1] = dat/100%10;
    temp[2] = dat/10%10;
    temp[3] = dat%10;
        
    if(temp[0] == 0)
    {        
        gauc_display_table[0] = segment7_decoded_table[no_seg];    //This bit is blank        
    }else
    {
        gauc_display_table[0] = segment7_decoded_table[temp[0]];
    }
	
	if(add == 2)
	{
	    gauc_display_table[1] = segment7_decoded_table[temp[1]] + Poit;
	}else
	{
	    gauc_display_table[1] = segment7_decoded_table[temp[1]];
	}
	if(add == 1)
	{
	    gauc_display_table[2] = segment7_decoded_table[temp[2]] + Poit;
	}else
	{
        gauc_display_table[2] = segment7_decoded_table[temp[2]];
	}
	gauc_display_table[3] = segment7_decoded_table[temp[3]];
}

/************************************************************************
* Function name: DisplayErr_2841
* Function Description: LED display EER
* Input parameters:
* Output parameters:
* return value :
*************************************************************************/
void DisplayErr_2841(void) //Digital tube display EER
{
      
    gauc_display_table[0] = segment7_decoded_table[no_seg];    //This bit is blank        
   
	gauc_display_table[1] = segment7_decoded_table[char_E];
	gauc_display_table[2] = segment7_decoded_table[char_r];
	gauc_display_table[3] = segment7_decoded_table[char_r];
}


/************************************************************************
* Function name: DisplayErr_2841
* Function Description: LED display EER
* Input parameters:
* Output parameters:
* return value :
*************************************************************************/
void DisplayFULL_2841(void) //?????EER
{
    gauc_display_table[0] = segment7_decoded_table[char_f];        
	gauc_display_table[1] = segment7_decoded_table[char_u];
	gauc_display_table[2] = segment7_decoded_table[char_l];
	gauc_display_table[3] = segment7_decoded_table[char_l];	
}

